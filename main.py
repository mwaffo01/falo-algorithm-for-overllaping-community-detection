#  Convert .dat into graph

import networkx as nx
import itertools
from cdlib import NodeClustering, algorithms, evaluation

def read_graph_from_dat_file(filename):
    """Reads a graph from a .dat file where each line represents an edge."""
    G = nx.Graph()
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split()
            if len(parts) == 2:
                u, v = map(int, parts)
                G.add_edge(u, v)
    return G
# Maximal cliques of size at least 3

def find_maximal_cliques(G):
    """Finds all maximal cliques in the graph."""
    return [clique for clique in nx.find_cliques(G) if len(clique) >= 3]

def write_cliques_to_file(cliques, filename):
    """Writes the cliques to a .clq file in the specified format."""
    with open(filename, 'w') as file:
        for index, clique in enumerate(cliques, start=1):
            # Join the elements of the clique with spaces, preceded by the index and a space
            clique_string = ' '.join(map(str, sorted(clique)))
            file.write(f"{index}  {clique_string}\n")


# THE BEST ONE FOR LATTICE AND  GROUP STRUCTURE


def read_cliques_from_file(filename):
    """Read cliques from a file into a list of sets."""
    with open(filename, 'r') as file:
        cliques = [set(map(int, line.strip().split()[1:])) for line in file]
    return cliques



def build_galois_lattice(cliques):
    """Construct the lattice structure from cliques, updating the last layer by removing elements found in the new layer."""
    L = [cliques]  # Initialize the lattice with the cliques as the first layer.
    k = 0  # Initialize layer counter.
    Lk = []
    LS = []
    max_level=k
    while True:
        new_layer = set()         
        label_sets = []

        # Generate the next layer by finding intersections.
        for i in range(len(L[-1])):
            for j in range(i + 1, len(L[-1])):
                intersection = L[-1][i].intersection(L[-1][j])
                if intersection:  # Ensure the intersection is not empty.
                    new_layer.add(frozenset(intersection))
        new_layer.update(Lk)
        Lk=[]
        
                # Prepare the next layer by filtering out subsets using a loop
        L_k = []
        
        #print("Lattice layer",new_layer)
        for n in new_layer:
            # Check if 'n' is not a subset of any other 'm' in 'new_layer'
            is_subset = False
            for m in new_layer:
                
                if n < m:
                    is_subset = True
                    Lk.append(frozenset(n))
                    break
            if not is_subset:
                L_k.append(set(n))



        U = set().union(*L_k)
        
        for n in L[-1]:           
            label_set = n - U
            if label_set:
                label_sets.append(n)
        LS.append(label_sets)         
                

        if not L_k:
            break  # Terminate if no new layer can be formed.

        
        L.append(L_k)  # Append the new layer to L
        k += 1  # Increment layer index.
        max_level=k    
    
    return L, max_level, LS

class UnionFind:
    def __init__(self, elements):
        self.parent = {element: element for element in elements}
        self.rank = {element: 0 for element in elements}

    def find(self, element):
        if self.parent[element] != element:
            self.parent[element] = self.find(self.parent[element])
        return self.parent[element]

    def union(self, element1, element2):
        root1 = self.find(element1)
        root2 = self.find(element2)

        if root1 != root2:
            if self.rank[root1] > self.rank[root2]:
                self.parent[root2] = root1
            elif self.rank[root1] < self.rank[root2]:
                self.parent[root1] = root2
            else:
                self.parent[root2] = root1
                self.rank[root1] += 1

def determine_group_structure(L, max_level, LS, n_common_elements=2):
    groups = [[] for _ in range(max_level+1)]

    for i in range(max_level+1):
        if i < len(L):
            # Combine all elements from L[i] and label sets from previous levels
            elements = [frozenset(s) for s in L[i]]
            for j in range(0, i + 1):
                elements.extend([frozenset(label) for label in LS[j]])

            uf = UnionFind(elements)

            # Perform unions on sets that have at least 'n_common_elements' common
            for idx, set1 in enumerate(elements):
                for set2 in elements[idx + 1:]:
                    if len(set1.intersection(set2)) >= n_common_elements:
                        uf.union(set1, set2)

            # Collect the groups based on union-find results
            group_dict = {}
            for element in elements:
                root = uf.find(element)
                if root in group_dict:
                    group_dict[root].update(element)
                else:
                    group_dict[root] = set(element)

            groups[i] = list(group_dict.values())

    return groups





def associate_unclassified_nodes(G, groups, L):
    """Associate unclassified nodes with existing groups based on adjacency."""
    classified_nodes1 = set().union(*[node for layer in L for node in layer])
    classified_nodes = [ str(node) for node in classified_nodes1]
    unclassified_nodes = set(G.nodes()) - set(classified_nodes)
    unclassified_nodes_as_int=set([ int(node) for node in unclassified_nodes])

    final_groups = groups.copy() # Copy of groups for modification

    for node in unclassified_nodes:

        neighbors = set(G.neighbors(node))

        neighbors_as_ints = {int(item) for item in neighbors}


        
        for layer_groups in final_groups:
            decision=0
            for group in layer_groups:

                if not neighbors_as_ints.isdisjoint(group-unclassified_nodes_as_int):
                    g=group
                    decision=decision+1

            if decision==1:
                layer_groups[layer_groups.index(g)].add(int(node))
   
    return final_groups



def find_unclassified_communities(G, classified_nodes):
    unclassified_nodes = set(G.nodes()) - set(classified_nodes)
    subgraph = G.subgraph(unclassified_nodes)

    # Trouver les composantes connexes du sous-graphe
    connected_components = list(nx.connected_components(subgraph))

    # Chaque composante connexe est une communauté supplémentaire
    comp_community = [component for component in connected_components]

    return comp_community





def calculate_eq(G, communities):
    m = G.size(weight='weight')  # total weight (or number) of edges in the graph
    A = nx.to_numpy_array(G)  # adjacency matrix as NumPy array
    degrees = dict(G.degree(weight='weight'))  # degree for each node
    node_list = list(G.nodes())  # list of nodes for indexing

    # Calculate O_i for each node i
    O = {node: 0 for node in G.nodes()}
    for community in communities:
        for node in community:
            O[node] += 1

    # Calculate extended modularity EQ
    EQ = 0
    for l, C_l in enumerate(communities):
        for i in C_l:
            for j in C_l:
                A_ij = A[node_list.index(i)][node_list.index(j)]
                EQ += (A_ij - degrees[i] * degrees[j] / (2 * m)) / (O[i] * O[j])

    EQ /= (2 * m)
    return EQ


def compute_modularities_and_best(G, final_communities):
    community_and_modularity = []
    best_modularity = (None, -float('inf'))  # Start with infinitely small modularity

    for communities in final_communities:
        modularity = calculate_eq(G, communities)
        community_and_modularity.append((communities, modularity))

        if modularity > best_modularity[1]:
            best_modularity = (communities, modularity)

    return community_and_modularity, best_modularity


# Import Ground Thruth


def read_community_file(filename):
    community_dict = {}
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split()
            node = parts[0]
            communities = parts[1:]
            for community in communities:
                if community not in community_dict:
                    community_dict[community] = []
                community_dict[community].append(node)
    # Convert dict to list of lists format
    return list(community_dict.values())

def compute_f_score(detected_communities, ground_truth_communities):
    f_scores = []

    # Function to calculate precision, recall, and F-score
    def f_score(detected_set, truth_set):
        if not detected_set or not truth_set:
            return 0
        precision = len(detected_set.intersection(truth_set)) / len(detected_set)
        recall = len(detected_set.intersection(truth_set)) / len(truth_set)
        if precision + recall == 0:
            return 0  # Avoid division by zero
        return 2 * (precision * recall) / (precision + recall)

    # For each detected community, find the best matching ground truth community
    for detected in detected_communities:
        max_f = max(f_score(detected, truth) for truth in ground_truth_communities)
        f_scores.append(max_f)
    
    # Return the average F-score if there are any, otherwise 0
    return sum(f_scores) / len(f_scores) if f_scores else 0

# Example usage with your data




def print_communities(community_dict):
    for index, nodes in enumerate(community_dict):
        print(f"Community {index+1}: Nodes {nodes}")



def main():
    # Define the path to your data file
    input_file_path = 'network.dat'

    # Create a new graph
    Gm2 = nx.Graph()

    # Read the edge list from the file
    # Assuming the file contains one edge per line, with nodes separated by a single space
    with open(input_file_path, 'r') as file:
        for line in file:
            # Split the line into two nodes
            nodes = line.strip().split()
            # Add an edge to the graph
            Gm2.add_edge(nodes[0], nodes[1])

    # Optionally, print some information about the graph
    print("Number of nodes:", Gm2.number_of_nodes())
    print("Number of edges:", Gm2.number_of_edges())

    input_filename = input_file_path
    output_filename = "network.clq"
    
    # Assuming functions for the operations below are defined somewhere in your script
    G = read_graph_from_dat_file(input_filename)
    cliques = find_maximal_cliques(G)
    write_cliques_to_file(cliques, output_filename)
    print(f"Maximal cliques of size at least 3 written to {output_filename}")

    # Usage
    filename = "network.clq"
    cliques = read_cliques_from_file(filename)
    L, max_level, LS = build_galois_lattice(cliques)
    n_common_elements = min(len(s) for s in L[-1]) + 1 if L[-1] else 2
    groups = determine_group_structure(L, max_level, LS, n_common_elements=2)
    final_groups = associate_unclassified_nodes(Gm2, groups, L)
    classified_final_nodes1 = set().union(*[node for layer in final_groups for node in layer])
    classified_final_nodes = [str(node) for node in classified_final_nodes1]
    unclassified_final_nodes = set(Gm2.nodes()) - set(classified_final_nodes)
    additional_communities = find_unclassified_communities(Gm2, classified_final_nodes)
    additional_communities_list = [{int(item) for item in community} for community in additional_communities]
    final_communities = [group + additional_communities_list for group in final_groups]
    final_communities_list = [
        [{str(item) for item in community} for community in level]
        for level in final_communities
    ]
    community_and_modularity, best_modularity = compute_modularities_and_best(Gm2, final_communities_list)
    best_community_without_outliers = [s for s in best_modularity[0] if len(s) > 2]
    best_community_without_outliers_list = [list(l) for l in best_community_without_outliers]

    # Assuming you have a graph object already loaded as 'G'
    file_name = 'community.dat'
    ground_truth_communities = read_community_file(file_name)
    ground_truth = NodeClustering(communities=ground_truth_communities, graph=Gm2, method_name="Ground Truth")
    your_community = NodeClustering(communities=best_community_without_outliers_list, graph=Gm2, method_name="Your Method")
    ground_truth_as_set = [set(s) for s in ground_truth.communities]
    onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(your_community, ground_truth)
    your_community_as_set = [set(s) for s in your_community.communities]
    average_f_score = compute_f_score(your_community_as_set, ground_truth_as_set)
    print(f"Average F-score: {average_f_score:.3f}")
    print("The overlapping modularity is", calculate_eq(Gm2, your_community_as_set))
    print("ONMI between my algorithm (FALO) and ground truth:", onmi_score.score)


if __name__ == "__main__":
    main()







